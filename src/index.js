import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import { injectGlobal } from 'styled-components';
import '../node_modules/material-components-web/dist/material-components-web.min.css'

injectGlobal`
  body {
    background: #e8e8e8;
    margin: 0;
    padding: 0;
    font-family: sans-serif;
  }
`

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
