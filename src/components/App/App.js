import React, { Component } from 'react';
import { SideBar, ViewPort, NavLogo, Content } from './styles';
import logo from '../../assets/logo.svg';
import DashboardLayout from '../DashboardLayout';

class App extends Component {
  render() {
    return (
      <ViewPort>
        <SideBar>
          <NavLogo src={logo} />
        </SideBar>
        <Content>
          <DashboardLayout />
        </Content>
      </ViewPort>
    );
  }
}

export default App;
