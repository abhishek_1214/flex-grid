import styled from 'styled-components';

export const ViewPort = styled.div`
  display: -webkit-flex;
  display: flex;
  flex-direction: row;
  width: 100vw;
  height: 100vh;
`

export const SideBar = styled.div`
  background: #fff;
  width: 56px;
  height: 100%;
  border-right: 1px solid #dedede;
  text-align: center;
`

export const NavLogo = styled.img`
  width: 48px;
  height: 48px;
`

export const Content = styled.div`
  flex: 1;
`