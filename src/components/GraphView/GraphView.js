import React from 'react';
import { CardContent } from './styles';
import {
  BarChart,
  Bar,
  ResponsiveContainer,
  PieChart,
  Pie,
  LineChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Line,
  Legend 
} from 'recharts';

class GraphView extends React.Component {
  constructor() {
    super()
    this.renderGraph = this.renderGraph.bind(this)
  }

  generateDataObject(data) {
    return data.map((item, index) => {
      return {
        name: `value-${index + 1}`,
        hours: item
      }
    })
  }

  renderGraph(type) {
    switch (type) {
      case 'line':
        return (
          <LineChart data={this.generateDataObject(this.props.data)}>
            <CartesianGrid stroke="#ccc" />
            <XAxis dataKey="name" />
            <YAxis />
            <Legend />
            <Line type="monotone" dataKey="hours" stroke="#333" />
          </LineChart>
        );
      case 'bar':
        return (
          <BarChart data={this.generateDataObject(this.props.data)}>
            <CartesianGrid stroke="#ccc" />
            <XAxis dataKey="name" />
            <YAxis />
            <Legend />
            <Bar dataKey="hours" fill="#c7c7c7" />
          </BarChart>
        )
      case 'pie':
        return (
          <PieChart>
            <Pie data={this.generateDataObject(this.props.data)} dataKey="hours" nameKey="name" label />
          </PieChart>
        )
      default:
        return <div></div>;
    }
  }

  render() {
    return (
      <CardContent>
        <ResponsiveContainer height="100%" width="100%">
          {this.renderGraph(this.props.type)}
        </ResponsiveContainer>
      </CardContent>
    )
  }
}

export default GraphView;