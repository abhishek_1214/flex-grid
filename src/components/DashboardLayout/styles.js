import styled from 'styled-components';

export const DragHeader = styled.div`
  cursor: grab;
  -webkit-transition: background 0.4ms;
  background: #fafafa;
  padding: 8px 0px;
  border-bottom: 1px solid #c3c3c3;

  &:hover {
    background: #dedede;
  }
`

export const DataContent = styled.div`
  display: -webkit-flex;
  padding: 8px 16px;
  display: flex;
  flex-direction: column;
`

export const Label = styled.p`
  margin: 0;
  padding: 0;
  font-size: 1rem;
  font-weight: 500;
`

export const DataInputRow = styled.div`
  display: -webkit-flex;
  display: flex;
  flex-direction: row;
`

export const CardTitle = styled.p`
  margin: 0;
  padding: 0;
  font-size: 1rem;
  margin-left: 16px;
`

export const GraphContainer = styled.div`
  display: -webkit-flex;
  display: flex;
  flex: 1;
  padding: 16px;
`
