import React from 'react';
import { Responsive, WidthProvider } from 'react-grid-layout';
import { Card } from 'rmwc/Card';
import GraphView from '../GraphView';
import { 
  GraphContainer,
  CardTitle,
  DragHeader,
  DataContent,
  DataInputRow
} from './styles';

import '../../../node_modules/react-grid-layout/css/styles.css';
import '../../../node_modules/react-resizable/css/styles.css';
import { TextField } from 'rmwc';



const ResponsiveGrid = WidthProvider(Responsive);

class DashboardLayout extends React.Component {
  state = {
    mounted: false,
    editDialog: false,
    activeCardData: null,
    activeIndex: null,
    cards: [
      { key: 'line', type: 'line', data: [22, 25, 23], title: 'Line Graph' },
      { key: 'bar', type: 'bar', data: [22, 25, 23], title: 'Bar Graph' },
      { key: 'pie', type: 'pie', data: [22, 25, 23], title: 'Pie Chart' },
    ]
  }

  constructor() {
    super()
    this.saveDataChanges = this.saveDataChanges.bind(this)
  }

  componentDidMount() {
    this.setState({
      mounted: true
    })
  }

  openEditDialog(index) {
    this.setState({
      activeCardData: this.state.cards[index],
      editDialog: true,
      activeIndex: index
    })
  }

  saveDataChanges(data) {
    let newCards = this.state.cards.map((item, index) => {
      if (index === this.state.activeIndex) {
        return data
      }
      return item;
    })

    console.log(newCards)
    this.setState({ cards: newCards })
  }

  updateData(event, itemIndex, valueIndex) {
    let newCardData = this.state.cards.map((item, index) => {
      if (index === itemIndex) {
        let newData = item.data.map((data, dataIndex) => {
          if (dataIndex === valueIndex) {
            return parseInt(event.target.value, 10)
          }
          return data;
        })

        return {
          ...item,
          data: newData
        }
      }
      return item;
    })

    this.setState({ cards: newCardData })
  }

  render() {
    var layouts = {
      lg: [
        {i: 'line', x: 0, y: 0, w: 6, h: 5, static: false},
        {i: 'bar', x: 6, y: 0, w: 3, h: 5, minW: 2, maxW: 8, isResizable: true},
        {i: 'pie', x: 9, y: 0, w: 3, h: 5, minW: 2, maxW: 8, isResizable: true},
      ]
    }

    return (
      <div>
        <ResponsiveGrid
          rowHeight={72}
          draggableHandle=".dragHandle"
          measureBeforeMount={false}
          useCSSTransforms={this.state.mounted}
          className="layout" 
          layouts={layouts}
        >
          {this.state.cards.map((item, index) => 
            <Card key={item.key}>
              <DragHeader className="dragHandle">
                <CardTitle>{item.title}</CardTitle>
              </DragHeader>
              <DataContent>
                <DataInputRow>
                  {item.data.map((val, dataIndex) => 
                    <TextField
                      key={dataIndex}
                      onChange={e => this.updateData(e, index, dataIndex)}
                      outlined
                      type="number"
                      label={`value-${dataIndex + 1}`}
                      value={val}
                    />
                  )}
                </DataInputRow>
              </DataContent>
              <GraphContainer>
                <GraphView type={item.type} data={item.data} />
              </GraphContainer>
            </Card>
          )}
        </ResponsiveGrid>
      </div>
    )
  }
}

export default DashboardLayout;